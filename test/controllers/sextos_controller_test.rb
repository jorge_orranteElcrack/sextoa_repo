require 'test_helper'

class SextosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sexto = sextos(:one)
  end

  test "should get index" do
    get sextos_url
    assert_response :success
  end

  test "should get new" do
    get new_sexto_url
    assert_response :success
  end

  test "should create sexto" do
    assert_difference('Sexto.count') do
      post sextos_url, params: { sexto: { age: @sexto.age, bio: @sexto.bio, name: @sexto.name } }
    end

    assert_redirected_to sexto_url(Sexto.last)
  end

  test "should show sexto" do
    get sexto_url(@sexto)
    assert_response :success
  end

  test "should get edit" do
    get edit_sexto_url(@sexto)
    assert_response :success
  end

  test "should update sexto" do
    patch sexto_url(@sexto), params: { sexto: { age: @sexto.age, bio: @sexto.bio, name: @sexto.name } }
    assert_redirected_to sexto_url(@sexto)
  end

  test "should destroy sexto" do
    assert_difference('Sexto.count', -1) do
      delete sexto_url(@sexto)
    end

    assert_redirected_to sextos_url
  end
end
