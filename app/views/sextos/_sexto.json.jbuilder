json.extract! sexto, :id, :name, :bio, :age, :created_at, :updated_at
json.url sexto_url(sexto, format: :json)
