class SextosController < ApplicationController
  before_action :set_sexto, only: [:show, :edit, :update, :destroy]

  # GET /sextos
  # GET /sextos.json
  def index
    @sextos = Sexto.all
  end

  # GET /sextos/1
  # GET /sextos/1.json
  def show
  end

  # GET /sextos/new
  def new
    @sexto = Sexto.new
  end

  # GET /sextos/1/edit
  def edit
  end

  # POST /sextos
  # POST /sextos.json
  def create
    @sexto = Sexto.new(sexto_params)

    respond_to do |format|
      if @sexto.save
        format.html { redirect_to @sexto, notice: 'Sexto was successfully created.' }
        format.json { render :show, status: :created, location: @sexto }
      else
        format.html { render :new }
        format.json { render json: @sexto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sextos/1
  # PATCH/PUT /sextos/1.json
  def update
    respond_to do |format|
      if @sexto.update(sexto_params)
        format.html { redirect_to @sexto, notice: 'Sexto was successfully updated.' }
        format.json { render :show, status: :ok, location: @sexto }
      else
        format.html { render :edit }
        format.json { render json: @sexto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sextos/1
  # DELETE /sextos/1.json
  def destroy
    @sexto.destroy
    respond_to do |format|
      format.html { redirect_to sextos_url, notice: 'Sexto was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sexto
      @sexto = Sexto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sexto_params
      params.require(:sexto).permit(:name, :bio, :age)
    end
end
